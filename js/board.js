function completeInsertComment(ret_obj) {
    var document_srl = ret_obj.document_srl;
    var comment_srl = ret_obj.comment_srl;
    jQuery('.feedback',parent.document).load('/' + document_srl + ' .feedback', function() {
        jQuery('html, body').animate({ scrollTop : jQuery('#comment_' + comment_srl).offset().top }, 350);
    }).fadeIn('slow' , function() {
        setTimeout("jQuery('textarea').val('')", 1000);
    });
}